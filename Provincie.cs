﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._4
{
    internal class Provincie
    {
        private List<Gemeente> _gemeentes = new List<Gemeente>();
        private string _provincienaam;

        public Provincie(string provincie)
        {
            Provincienaam = provincie;
        }
        public string Provincienaam 
        {
            get { return _provincienaam; }
            set { _provincienaam = value;}
        }
        private List<Gemeente> Gemeentes 
        {
            get { return _gemeentes; } 
            set { _gemeentes = value; }
        }
        public void addGemeente (Gemeente gemeente)
        {
            Gemeentes.Add(gemeente);
        }
        public void RemoveGemeente(Gemeente gemeente)
        {
            Gemeentes.Remove(gemeente);
        }
        public override string ToString()
        {
            string lijstGemeentes = "Provincie " + Provincienaam + Environment.NewLine;

            foreach (Gemeente gemeente in Gemeentes)
            {
                lijstGemeentes += gemeente.ToString() + Environment.NewLine;
            }

            return lijstGemeentes;
        }
    }
}
