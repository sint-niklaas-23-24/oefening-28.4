﻿namespace Oefening_28._4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Provincie antwerpen = new Provincie("Antwerpen");
            Gemeente kasterlee = new Gemeente("2460", "Kasterlee");
            Gemeente geel = new Gemeente("2440", "Geel");
            Gemeente brussel = new Gemeente("1000", "Brussel");
            Gemeente lille = new Gemeente("2275", "Lille");
            antwerpen.addGemeente(kasterlee);
            antwerpen.addGemeente(geel);
            antwerpen.addGemeente(brussel);
            antwerpen.addGemeente(lille);

            Console.WriteLine(antwerpen.ToString());
            Console.WriteLine();

            antwerpen.RemoveGemeente(brussel);
            Console.WriteLine("Na het verwijderen van Brussel");
            Console.WriteLine(antwerpen.ToString());
            Console.ReadKey();
        }
    }
}