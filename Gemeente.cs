﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._4
{
    internal class Gemeente
    {
        private string _gemeenteNaam;
        private string _postcode;

        public Gemeente(string postcode, string gemeente) 
        {
            Postcode = postcode;
            GemeenteNaam = gemeente;
        }
        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }
        public string GemeenteNaam
        {
            get { return _gemeenteNaam; }
            set { _gemeenteNaam = value; }
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Gemeente r = (Gemeente)obj;
                    if (this.Postcode == r.Postcode)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {
            return Postcode + " - " + GemeenteNaam;
        }
    }
}
